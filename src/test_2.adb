with Ada.Assertions;
with Ada.Text_IO;
with Interfaces;
with Sha256;
with Debug_Print;

use Ada.Assertions;
use Ada.Text_IO;
use Interfaces;
use Sha256;
use Debug_Print;

procedure Test_2 is
	
	input_data : InputData := (
		16#61#, 16#62#, 16#63#, 16#64#, 16#62#, 16#63#, 16#64#, 16#65#, 
		16#63#, 16#64#, 16#65#, 16#66#, 16#64#, 16#65#, 16#66#, 16#67#, 
		16#65#, 16#66#, 16#67#, 16#68#, 16#66#, 16#67#, 16#68#, 16#69#, 
		16#67#, 16#68#, 16#69#, 16#6a#, 16#68#, 16#69#, 16#6a#, 16#6b#, 
		16#69#, 16#6a#, 16#6b#, 16#6c#, 16#6a#, 16#6b#, 16#6c#, 16#6d#, 
		16#6b#, 16#6c#, 16#6d#, 16#6e#, 16#6c#, 16#6d#, 16#6e#, 16#6f#, 
		16#6d#, 16#6e#, 16#6f#, 16#70#, 16#6e#, 16#6f#, 16#70#, 16#71#
	);

	computed : Hashsum := (0, 0, 0, 0, 0, 0, 0, 0);
	expected : constant Hashsum := (
		16#248d6a61#, 
		16#d20638b8#, 
		16#e5c02693#, 
		16#0c3e6039#, 
		16#a33ce459#, 
		16#64ff2167#, 
		16#f6ecedd4#, 
		16#19db06c1#
	);

begin	

	Put_Line("Input size: " & Integer'Image(input_data'length));

	computed := Compute(input_data);

	Put_Line("Computed: ");	
	for i in HashsumRange loop
		Put_Hex(computed(i));
	end loop;
	
	Put_Line("");	
	Put_Line("Expected: ");	
	for i in HashsumRange'First .. HashsumRange'Last loop
		Put_Hex(expected(i));
	end loop;

	Assert(true = Compare(expected, computed), "computed /= expected (Compare(expected, computed)= " & Boolean'Image(Compare(expected, computed)) );

end Test_2;
