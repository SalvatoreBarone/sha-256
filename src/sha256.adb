with Interfaces;
with Ada.Assertions;

package body Sha256 is
	use Ada.Assertions;
	use Interfaces;
		
	type ArrayOfUnsigned_32 is array (Natural range <>) of Unsigned_32;
	
	K : constant ArrayOfUnsigned_32 := (
		16#428a2f98#, 16#71374491#, 16#b5c0fbcf#, 16#e9b5dba5#, 
		16#3956c25b#, 16#59f111f1#, 16#923f82a4#, 16#ab1c5ed5#,
		16#d807aa98#, 16#12835b01#, 16#243185be#, 16#550c7dc3#, 
		16#72be5d74#, 16#80deb1fe#, 16#9bdc06a7#, 16#c19bf174#,
		16#e49b69c1#, 16#efbe4786#, 16#0fc19dc6#, 16#240ca1cc#, 
		16#2de92c6f#, 16#4a7484aa#, 16#5cb0a9dc#, 16#76f988da#,
		16#983e5152#, 16#a831c66d#, 16#b00327c8#, 16#bf597fc7#, 
		16#c6e00bf3#, 16#d5a79147#, 16#06ca6351#, 16#14292967#,
		16#27b70a85#, 16#2e1b2138#, 16#4d2c6dfc#, 16#53380d13#, 
		16#650a7354#, 16#766a0abb#, 16#81c2c92e#, 16#92722c85#,
		16#a2bfe8a1#, 16#a81a664b#, 16#c24b8b70#, 16#c76c51a3#, 
		16#d192e819#, 16#d6990624#, 16#f40e3585#, 16#106aa070#,
		16#19a4c116#, 16#1e376c08#, 16#2748774c#, 16#34b0bcb5#, 
		16#391c0cb3#, 16#4ed8aa4a#, 16#5b9cca4f#, 16#682e6ff3#,
		16#748f82ee#, 16#78a5636f#, 16#84c87814#, 16#8cc70208#, 
		16#90befffa#, 16#a4506ceb#, 16#bef9a3f7#, 16#c67178f2#);

	function RotateWsA(word : in Unsigned_32) return Unsigned_32;
	
	function RotateWsB(word : in Unsigned_32) return Unsigned_32;
	
	function RotateRA(word : in Unsigned_32) return Unsigned_32;
	
	function RotateRB(word : in Unsigned_32) return Unsigned_32;
	
	function Ch(a, b, c : in Unsigned_32) return Unsigned_32;

	function Maj(a, b, c : in Unsigned_32) return Unsigned_32;

	procedure WordScheduling ( 
		data_block : in ArrayOfUnsigned_32;
		scheduled_words : out ArrayOfUnsigned_32);

	procedure SingleRound (
		data_buffer : in out Hashsum;
		Wt : Unsigned_32;
		Kt : Unsigned_32);

	procedure FBox (
		data_block : in ArrayOfUnsigned_32;
		chVar : in Hashsum;
		tmp_hash: out Hashsum);

	function Compute(input_data : in InputData) return Hashsum is

		
		-- Fractional part of the square root of the first 8 prime numbers.
		chVars : Hashsum := (
			16#6A09E667#, 16#BB67AE85#,	16#3C6EF372#, 16#A54FF53A#,
			16#510E527F#, 16#9B05688C#, 16#1F83D9AB#, 16#5BE0CD19#);

		-- This varialbe will contain the computed hashsum
		hash : Hashsum := (0, 0, 0, 0, 0, 0, 0, 0);

		padding : Unsigned_32 := 16#80#;
		
		-- Numberof padding bytes
		padding_bytes : Integer := 0;


		-- Total data length, padding included.
		total_length : Integer := 0;

		-- SHA-256 processes 512 bits (16 words, each 32-bits logg) at a time.
		-- This variable holds the currently under process data picked from 
		-- input array.
		data_block : ArrayOfUnsigned_32 (0 .. 15);

		-- input data loop counters
		i , data_i : Integer;

	begin
		
		-- Computing the total length of the data, padding included.
		if input_data'length mod 64 = 56 then
			padding_bytes := 64;
		else
			padding_bytes := 56 - (input_data'length mod 64);
		end if;
		total_length := input_data'length + padding_bytes + 8;

		-- loop on the input data
		i := 3;
		data_i := 0;
		while i < total_length loop
			-- Copying input data into temporary data block, a 4-byte word at a 
			-- time and padding on the fly
			data_block(data_i) := 0;
			for j in 0 .. 3 loop
				declare
					tmp : Unsigned_32;
					sh_amount : Natural := Natural(j)*8;
				begin
					if (i - j) < input_data'length  then
						tmp := Unsigned_32(input_data(i - j));
					elsif (i - j) = input_data'length then
						tmp := padding;
					else
						tmp := 0;
					end if;
					data_block(data_i) := data_block(data_i) or tmp;
					data_block(data_i) := Rotate_Right(data_block(data_i), 8);
				end;
			end loop;
			data_i := data_i + 1;

			-- If the temporary data buffer is full, we are ready for FBox
			-- computation
			if (i + 1) mod 64 = 0 then
				-- If it is the last partition, we need to append the total 
				-- length
				if i = total_length - 1 then
					data_block(14) := Shift_Right(input_data'length, 29);
					data_block(15) := Shift_Left(input_data'length, 3);
				else
					data_i := 0;
				end if;

				FBox(data_block, chVars, hash);

				for j in HashsumRange loop
					chVars(j) := hash(j);
				end loop;

			end if;


			i := i + 4;
		end loop;

		return hash;

	end Compute;

	----------------------------------------------------------------------------
	--
	function Compare(hash1, hash2: in Hashsum) return Boolean is
	begin
		for i in HashsumRange loop
			if hash1(i) /= hash2(i) then
				return false;
			end if;
		end loop;
		return true;
	end Compare;

	----------------------------------------------------------------------------
	--
	function RotateWsA(word : in Unsigned_32) return Unsigned_32 is
	begin
		return Rotate_Right(word, 7) xor Rotate_Right(word, 18) xor Shift_Right(word, 3);
	end RotateWsA;

	----------------------------------------------------------------------------
	--
	function RotateWsB(word : in Unsigned_32) return Unsigned_32 is
	begin
		return Rotate_Right(word, 17) xor Rotate_Right(word, 19) xor Shift_Right(word, 10);
	end RotateWsB;
	
	----------------------------------------------------------------------------
	--
	function RotateRA(word : in Unsigned_32) return Unsigned_32 is
	begin
		return Rotate_Right(word, 2) xor Rotate_Right(word, 13) xor Rotate_Right(word, 22);
	end RotateRA;
	
	----------------------------------------------------------------------------
	--
	function RotateRB(word : in Unsigned_32) return Unsigned_32 is
	begin
		return Rotate_Right(word, 6) xor Rotate_Right(word, 11) xor Rotate_Right(word, 25);
	end RotateRB;

	----------------------------------------------------------------------------
	--
	function Ch(a, b, c : in Unsigned_32) return Unsigned_32 is
	begin
		return (a and b) xor ((not a) and c);
	end Ch;
	
	----------------------------------------------------------------------------
	--
	function Maj(a, b, c : in Unsigned_32) return Unsigned_32 is
	begin
		return (a and b) xor (a and c) xor (b and c);
	end Maj;

	----------------------------------------------------------------------------
	--
	procedure WordScheduling ( 
		data_block : in ArrayOfUnsigned_32;
		scheduled_words : out ArrayOfUnsigned_32) is
	begin
		for i in 0 .. 15 loop
			scheduled_words(i) := data_block(i);
		end loop;

		for i in 16 .. 63 loop
			scheduled_words(i) := 	RotateWsB(scheduled_words(i - 2)) +
									scheduled_words(i - 7) +
									RotateWsA(scheduled_words(i - 15)) +
									scheduled_words(i - 16);
		end loop;
	end WordScheduling;

	----------------------------------------------------------------------------
	--
	procedure SingleRound (
		data_buffer : in out Hashsum;
		Wt : Unsigned_32;
		Kt : Unsigned_32) is

		T1 : Unsigned_32;
		T2 : Unsigned_32;

	begin

		T1 := 	data_buffer(HashsumRange'Last) + 
				Ch(data_buffer(HashsumRange'Last - 3), data_buffer(HashsumRange'Last - 2), data_buffer(HashsumRange'Last - 1)) +
				RotateRB(data_buffer(HashsumRange'Last - 3)) +
				Wt +
				Kt;

		T2 := 	RotateRA(data_buffer(HashsumRange'First)) +
				Maj(data_buffer(HashsumRange'First), data_buffer(HashsumRange'First + 1), data_buffer(HashsumRange'First + 2));

		for i in reverse Hashsum'First + 1 .. Hashsum'Last loop
			data_buffer(i) := data_buffer(i - 1);
		end loop;
		data_buffer(HashsumRange'First + 4) := data_buffer(HashsumRange'First + 4) + T1;
		data_buffer(HashsumRange'First) := T1 + T2;

	end SingleRound;

	----------------------------------------------------------------------------
	--
	procedure FBox (
		data_block : in ArrayOfUnsigned_32;
		chVar : in Hashsum;
		tmp_hash: out Hashsum) is
		
		W : ArrayOfUnsigned_32 (0 .. 63);

	begin
		
		WordScheduling(data_block, W);

		for i in HashsumRange loop
			tmp_hash(i) := chVar(i);
		end loop;

		for i in 0 .. 63 loop
			SingleRound(tmp_hash, W(i), K(i));
		end loop;

		for i in HashsumRange loop
			tmp_hash(i) := tmp_hash(i) + chVar(i);
		end loop;

	end FBox;

end Sha256;
