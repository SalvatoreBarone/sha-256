with Ada.Assertions;
with Ada.Text_IO;
with Interfaces;
with Sha256;
with Debug_Print;

use Ada.Assertions;
use Ada.Text_IO;
use Interfaces;
use Sha256;
use Debug_Print;

procedure Test_1 is
	
	input_data : InputData := (
		16#61#, 16#62#, 16#63#
	);

	computed : Hashsum := (others => 0);
	expected : constant Hashsum := (
		16#ba7816bf#,
		16#8f01cfea#, 
		16#414140de#, 
		16#5dae2223#, 
		16#b00361a3#, 
		16#96177a9c#, 
		16#b410ff61#, 
		16#f20015ad#	
	);

begin

	Put_Line("Input size: " & Integer'Image(input_data'length));

	computed := Compute(input_data);

	Put_Line("Computed: ");	
	for i in HashsumRange loop
		Put_Hex(computed(i));
	end loop;
	
	Put_Line("");	
	Put_Line("Expected: ");	
	for i in HashsumRange'First .. HashsumRange'Last loop
		Put_Hex(expected(i));
	end loop;

	Assert(true = Compare(expected, computed), "computed /= expected (Compare(expected, computed)= " & Boolean'Image(Compare(expected, computed)) );

end Test_1;
