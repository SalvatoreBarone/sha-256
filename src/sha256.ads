with Interfaces;

package Sha256 is
	use Interfaces;

	----------------------------------------------------------------------------
	-- DATA TYPES DEFINITION
	----------------------------------------------------------------------------

	-- Definition of the "input data" data type.
	type InputData is array (Natural range <>) of Unsigned_8;

	-- Definition of range for the hashsum data type.
	type HashsumRange is range 0 .. 7;

	-- Hashsum data type.
	type Hashsum is array (HashsumRange) of Unsigned_32;

	
	----------------------------------------------------------------------------
	-- FUNCTIONS/PROCEDURE  DEFINITION
	----------------------------------------------------------------------------

	-- Given an input array that contains data, the function computes and
	-- returns its SHA-256 hashsum.
	function Compute(input_data : in InputData) return Hashsum;

	function Compare(hash1, hash2: in Hashsum) return Boolean;

end Sha256;
