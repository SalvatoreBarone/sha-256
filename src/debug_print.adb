with Interfaces;
with Ada.Text_IO;
with Ada.Integer_Text_IO;

package body Debug_Print is
	use Interfaces;
	use Ada.Text_io;
	use Ada.Integer_Text_IO;

	procedure Put_Hex(word : Unsigned_32) is
		
		package Unsigned_32_IO is new Integer_IO (Integer);
		
		word_h, word_l : Unsigned_32;

	begin
		Unsigned_32_IO.Default_Width := 9;
		Unsigned_32_IO.Default_Base := 16;

		word_h := word and 16#ffff0000#;
		word_h := Shift_Right(word_h, 16); 
		word_l := word and 16#0000ffff#;

		Unsigned_32_IO.Put(Integer(word_h));
		Unsigned_32_IO.Put(Integer(word_l));

	end Put_Hex;
end Debug_Print;
