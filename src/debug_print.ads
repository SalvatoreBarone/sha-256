with Interfaces;

package Debug_Print is
	use Interfaces;

	procedure Put_Hex(word : Unsigned_32);

end Debug_Print;
