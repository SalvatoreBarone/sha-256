# Naive ADA implementation of the SHA-256 secure hash function

## Compilation

In order to compile, you need an ADA compiler, such as GNU/gcc-gnat.
Please, install GNAT using your package manager, and, then, type the following:

```
cd src
gprbuild -P build.gpr --create-missing-dirs
```
